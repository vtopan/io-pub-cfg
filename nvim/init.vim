if $MYVIMRC =~ ":"
    let g:platform = "win"
else    
    let g:platform = "lin"
endif
exec "source $MYVIMRC." . g:platform . ".vim"


source $MYVIMRC.general.vim
source $MYVIMRC.shortcuts.vim
source $MYVIMRC.plugins.vim

" uncomment this after adding plugins
"silent! helptags ALL 

if filereadable(expand("$MYVIMRC." . g:platform . "-post.vim"))
    exec "source $MYVIMRC." . g:platform . "-post.vim"
endif
