"""--- general settings ---

" explicit encoding
set encoding=utf-8
" text width
set tw=100
" marker (color) column
set cc=101
" tab
set tabstop=4 softtabstop=0 expandtab shiftwidth=4
" clipboard
set clipboard=unnamed,unnamedplus
" line numbers
set number
set relativenumber
" highlight current line
set cursorline
" wrap on insert
set fo-=l
" palette
colorscheme railscasts
set background=dark
" case insensitive search (except for capitals)
set ignorecase
set smartcase
" don't jump to start of line after some actions
set nostartofline
" ask instead of failing
set confirm
" set command height to 2
set cmdheight=3
" quickly time out on keycodes, never on mappings
set notimeout ttimeout ttimeoutlen=200
" toggle paste/nopaste with F11
set pastetoggle=<F11>
" enable modeline
set modeline
set modelines=2
" don't insert extra space when joining lines
setl nojs
" spelling
"set spellfile=$VIMRUNTIME/spell/en.utf-8.add
" highlight tabs, trailing spaces
""set list listchars=tab:→\ ,trail:·,nbsp:·
"set list listchars=tab:>-,trail:.
" don't select first item, follow typing in autocomplete
set completeopt=menuone,longest,preview
" minimum lines to keep above and below cursor
set scrolloff=3
" automatically save views (code folds)
"autocmd BufWinLeave ?* mkview
"autocmd BufWinEnter ?* silent loadview
" show hex code of binary chars instead of @...
set display=uhex,lastline
" open new splits on the right side
set splitright

""" extra colors

" esearch
hi ESearchMatch ctermfg=black ctermbg=white guifg=#FFFFFF guibg=#333333

" go to last file position
autocmd BufReadPost * if @% !~# '\.git[\/\\]COMMIT_EDITMSG$' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g`\"" | endif 


"""--- per-filetype ----

"au BufNewFile,BufRead *.{md,mdwn,mkd,mkdn,mark*} set filetype=markdown
au BufNewFile,BufRead *.py set ts=4 sts=4 sw=4 tw=100 et ai ff=unix
au BufNewFile,BufRead *.js,*.html,*.css,*.yaml,*.json,*.rqnetop set ts=2 sts=2 sw=2
" remove trailing whitespace
au BufWritePre *.sh,*.py,*.c,*.h,*.cpp,*.hpp,*.java,*.md,Makefile*,*.htm*,*.css,*.js,*.asm,*.tex,*.yaml,*.json,*.rqnetop %s/\s\+$//e
au BufNewFile,BufRead *.bat,*.cmd set ff=dos
au BufNewFile,BufRead *.rqnetop set filetype=json ff=unix
au BufNewFile,BufRead *.md set fo=tacqw ts=4 sts=4 sw=4 tw=100 et ai

" run
autocmd filetype python nnoremap <F5> :w <bar> exec '!python '.shellescape('%')<CR>
" compile
autocmd filetype cpp nnoremap <F5> :w <bar> exec '!g++ '.shellescape('%').' -o '.shellescape('%:r').' && '.shellescape('%:r')<CR>
" ?
autocmd filetype html,css EmmetInstall
