"""--- plugin configuration --------------------------------------------------------------------------


""" tablemode
let g:table_mode_corner='|'

""" markdown
"let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_folding_style_pythonic = 1
let g:vim_markdown_no_extensions_in_markdown = 1
let g:vim_markdown_autowrite = 0

""" airline
let g:airline_theme='distinguished'
let g:airline_skip_empty_sections = 1
"let g:airline_powerline_fonts = 1
"let g:airline_section_c = '%t %{gutentags#statusline()}'
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#branch#empty_message = 'no-git?'
let g:airline#extensions#branch#sha1_len = 10
let g:airline#extensions#bufferline#enabled = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline#extensions#hunks#enabled = 1
let g:airline#extensions#gutentags#enabled = 1
let g:airline#extensions#ale#enabled = 1

""" ctrlp
let g:ctrlp_root_markers = ['pom.xml', '.p4ignore', '.git', '.hg', '.svn', '.bzr', '_darcs']
let g:ctrlp_cmd = 'CtrlPMixed'
set wildignore+=*.pyc,venv,*.png,.git
set wildignore+=*_build/*
set wildignore+=*/coverage/*

""" cppman
autocmd FileType c,cpp set keywordprg=cppman

""" incsearch+fuzzy
map /  <Plug>(incsearch-forward)
map ?  <Plug>(incsearch-backward)
map z/ <Plug>(incsearch-fuzzyspell-/)

""" ultisnips
let g:UltiSnipsExpandTrigger="<c-u>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"

""" json
let g:vim_json_syntax_conceal = 0

""" esearch
let g:esearch_backend = 'nvim'
let g:esearch_adapter = 'rg'
let g:esearch_adapter = 'win'   " or 'qflist'

""" pandoc
let g:pandoc#syntax#conceal#use = 0
let g:pandoc#formatting#textwidth = 120
let g:pandoc#folding#level = 3
let g:pandoc#folding#mode = "relative"  
let g:pandoc#keyboard#sections#header_style = "s"
let g:pandoc#modules#disabled = ["formatting"]
"let g:pandoc#formatting#mode = "hA"
"let g:pandoc#formatting#equalprg = ''

"todo: add macro: PandocTemplate save fancy_article pdf -Vdocumentclass=memoir -Vmainfont='Helvetica Neue'

""" nerd commenter
"let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDTrimTrailingWhitespace = 1
let g:NERDToggleCheckAllLines = 1

""" ale
"let g:ale_fixers = {
"    \ '*': ['remove_trailing_lines', 'trim_whitespace'],
"    \ 'javascript': ['prettier', 'eslint'],
"    \ 'python': ['autopep8', 'isort', 'yapf'],
"    \}
let g:ale_linters = {
    \ 'python': ['flake8'],  
    \}
"    \ 'cpp': ['clang-format', 'clang-tidy', 'clang-check'],
"todo: fix fixers/linters

let g:ale_python_flake8_options = '--ignore C901,E303,E127,E128,E266,E231,W391,W503,E123 --max-line-length=120'
" C901=too complex - fix this by tweaking the max score (see man flake8)
" E303=too many blank lines; E127/8=cont. line over/under-indented; E266=too many leading #; E231: space after :;
" E123=closing bracket doesn't match opening indent
" W391=blank line at EOF; W503=line break before binary op

let g:ale_lint_on_text_changed = 0
let g:ale_lint_on_insert_leave = 1
let g:ale_lint_on_save = 1    
let g:ale_set_highlights = 1
let g:ale_completion_enabled = 0

"let g:ale_sign_column_always = 1    
"let g:ale_open_list = 1
"let g:ale_keep_list_window_open = 1

""" deoplete
let g:deoplete#enable_at_startup = 1    

""" vim-clang
let g:clang_c_options = '-Wall'
let g:clang_cpp_options = '-Wall -Xclang -flto-visibility-public-std'
"let g:clang_dotfile = '.clang'

""" neosnippet
let g:neosnippet#enable_completed_snippet = 1  

""" jedi-vim
let g:jedi#completions_enabled = 1

""" autoformat
"let g:formatdef_c = '"astyle --style=bsd -s4 -xk -xl -C -K -w -U -p -H -j -c -xy -xC120 --mode=c -z2 -k1"'
"let g:formatdef_io_clang='"clang-format -style=file"'
let g:formatters_c = ['clangformat']
let g:formatters_cpp = ['clangformat']
let g:formatters_py = ['yapf']
let g:formatter_yapf_style = $VIMRUNTIME . '/io-cfg/.style.yapf'
"let g:autoformat_verbosemode=1
        
""" nim    
let g:nvim_nim_enable_async = has("nvim")

""" emmet
let g:user_emmet_install_global = 0
let g:user_emmet_leader_key='<leader>h'
