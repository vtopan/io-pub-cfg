"""--- command aliases ------------------------------------------------------------------------------------

command Spell setlocal spell spelllang=en_us
command Spellro setlocal spell spelllang=ro_ro
command SpellRo setlocal spell spelllang=ro_ro
command Nospell setlocal nospell
command Ss mks! .sess.vim
command SS mks! .sess.vim
command Os so .sess.vim
command OS so .sess.vim

" font
command FC GuiFont Consolas:h11
command FU GuiFont Ubuntu Mono:h11
command FD GuiFont DejaVu Sans Mono:h10
command FA GuiFont agave:h12


"""--- shortcuts ------------------------------------------------------------------------------------

let g:mapleader = ","
let maplocalleader = ","

inoremap <silent> <Esc> <Esc>`^

nnoremap H ^
nnoremap L $

" replace tabs with 4 spaces
nnoremap <leader>rts :%s/	/    /g<CR>
nnoremap <leader>eolu %s/\r//g
nnoremap <leader>eold %s/\n/\r\n/g

" clipboard
nnoremap <S-Insert> P
inoremap <S-Insert> <Esc>lPl<Ins>

" mark spot before search (return with `s)
nnoremap / ms/
nnoremap ? ms?
nnoremap f msf

" save
inoremap <F2> <Esc>:w<CR>
nnoremap <F2> :w<CR>

" make Y go to end of line
map Y y$

" C-L clears highlight
nnoremap <C-L> :nohl<CR><C-L>

" pgup/dn don't move caret - doesn't work?
nnoremap <silent> <PageUp> <C-U><C-U>
vnoremap <silent> <PageUp> <C-U><C-U>
inoremap <silent> <PageUp> <C-\><C-O><C-U><C-\><C-O><C-U>
nnoremap <silent> <PageDown> <C-D><C-D>
vnoremap <silent> <PageDown> <C-D><C-D>
inoremap <silent> <PageDown> <C-\><C-O><C-D><C-\><C-O><C-D>

" todo/fixme
nnoremap <leader>? :g/TODO\\|FIXME\\|XXX/cexpr expand("%") . ":" . line(".") .  ":" . getline(".")<CR>

" tab, buffer & split navigation
nnoremap <C-Tab> gt
inoremap <C-t> <Esc>:tabnew<CR>:CtrlP<CR>
nnoremap <C-t> :tabnew<CR>:CtrlP<CR>
nnoremap <leader>\ :tabonly<CR>
nnoremap <A-1> 1gt
nnoremap <A-2> 2gt
nnoremap <A-3> 3gt
nnoremap <A-4> 4gt
nnoremap <A-5> 5gt
nnoremap <A-Left> <C-w>h
nnoremap <A-Down> <C-w>j
nnoremap <A-Up> <C-w>k
nnoremap <A-Right> <C-w>l
nnoremap <C-F2> <C-w>v<CR>:CtrlP<CR>
nnoremap <S-F2> <C-w>s<CR>:CtrlP<CR>
nnoremap <A-q> <Esc>:q<CR>
" open previous buffer in the right split
nnoremap <leader>sr :vert sbp<CR>

" split navigations
nnoremap <C-J> <C-W><C-J>
"nnoremap <C-K> <C-W><C-K>
nnoremap <C-H> <C-W><C-H>

" fix mistypes
command! Q q
command! Qa qa


"""--- plugin shortcuts ------------------------------------------------------------------------------------

" cscope
nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>	
nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>	
nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>	
nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>	
nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>	
nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>	
nmap <C-\>i :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>	 

" argwrap
nnoremap <silent> <leader>aw :ArgWrap<CR>

"if exists(":Tableize")
nnoremap <leader>tt :Tableize/<tab><CR>
nnoremap <leader>t<space> :Tableize/<space><CR>
nnoremap <leader>t2<space> :Tableize/<space><space>\+<CR>
nnoremap <leader>t, :Tableize/,<CR>
nnoremap <leader>t; :Tableize/;<CR>
nnoremap <leader>t<bar> :Tableize/<bar><CR>
"endif

nnoremap <F8> :TagbarToggle<CR>

"map <C-n> :NERDTreeToggle<CR>
nnoremap <leader>n :NERDTreeToggle<CR>

"if exists(":Tabularize")
nmap <leader>a= :Tabularize /=<CR>
vmap <leader>a= :Tabularize /=<CR>
nmap <leader>a: :Tabularize /:<CR>
vmap <leader>a: :Tabularize /:<CR>
nmap <leader>a:: :Tabularize /:\\zs<CR>
vmap <leader>a:: :Tabularize /:\\zs<CR>
nmap <leader>a, :Tabularize /,<CR>
vmap <leader>a, :Tabularize /,<CR>
nmap <leader>a\| :Tabularize /\|<CR>
vmap <leader>a\| :Tabularize /\|<CR>
"endif

" tablemode
nmap <leader>T :TableModeEnable<CR>

" pandoc
nnoremap <leader>pp :Pandoc! pdf -V geometry:paperwidth=210mm,paperheight=297mm,margin=1.5cm<CR>
nnoremap <leader>ph :Pandoc! html --mathml -s<CR>

" gen_tags
nnoremap GT :GenCtags<CR>

" nerd commenter
nnoremap <A-c> <Plug>NERDComToggleComment

" camelcasemotions
omap <silent> i<A-w> <Plug>CamelCaseMotion_iw
xmap <silent> i<A-w> <Plug>CamelCaseMotion_iw
map <silent> <A-w> <Plug>CamelCaseMotion_w
map <silent> <A-b> <Plug>CamelCaseMotion_b
map <silent> <A-e> <Plug>CamelCaseMotion_e
imap <silent> <A-Left> <C-o><Plug>CamelCaseMotion_b
imap <silent> <A-Right> <C-o><Plug>CamelCaseMotion_w

" autoformat
noremap <leader>fo :Autoformat<CR>

" neosnippet
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)

" fzf
nmap <C-S-f> :Rg<CR>
nmap <F3> :Files<CR>
nmap <leader>zf :Files<CR>
nmap <leader>zs :Rg<CR>
nmap <leader>zt :Tags<CR>
nmap <leader>zc :Commits<CR>
nmap <leader>zbc :BCommits<CR>
nmap <leader>zgf :GFiles<CR>
nmap <leader>zm :Maps<CR>
nmap <leader>zh :History/<CR>
nmap <leader>zgg :ZGgrep<space>
command! -bang -nargs=* ZGgrep
  \ call fzf#vim#grep(
  \   'git grep --ignore-case --line-number '.shellescape(<q-args>), 0,
  \   { 'dir': systemlist('git rev-parse --show-toplevel')[0] }, <bang>0)
imap <c-i>f <plug>(fzf-complete-file-ag)  
  
" fugitive
nmap <leader>gs :Gstatus<CR>
nmap <leader>gb :Gblame<CR>
nmap <leader>gd :Gdiff<CR>
nmap <leader>gc :Gcommit<CR>
nmap <leader>gcf :Gcommit %<CR>
nmap <leader>gl :Glog<CR>
nmap <leader>gg :Ggrep<space>

" vim-gitgutter
let g:gitgutter_sign_added = '++'
let g:gitgutter_sign_modified = '##'
let g:gitgutter_sign_removed = '--'
let g:gitgutter_sign_removed_first_line = '-v'
let g:gitgutter_sign_modified_removed = '+-'

nmap <leader>ht :GitGutterLineHighlightsToggle<CR>

" OverCommandLine
nnoremap / :OverCommandLine<CR>/
nnoremap <C-h> :OverCommandLine<CR>%s/
